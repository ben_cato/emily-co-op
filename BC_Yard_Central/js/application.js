﻿$(document).ready(function(){
  //initialize flexslider
  $('.calendar-slider, .calendar-picker').flexslider({
    animation: "slide",
    animationSpeed: 1100,
    slideshow: false,
    controlNav: false,
    directionNav: false
  });  

  //connect slider buttons to calendar slides
  $('.prev').on('click', function(){
      $('.calendar-slider').flexslider('prev')
      return false;
  })
  $('.next').on('click', function(){
      $('.calendar-slider').flexslider('next')
      return false;
  })

  //connect slider buttons to month name slides
  $('.prev').on('click', function(){
      $('.calendar-picker').flexslider('prev')
      return false;
  })
  $('.next').on('click', function(){
      $('.calendar-picker').flexslider('next')
      return false;
  })

  //dropdown for multiple events
  $('.dropdown').css('display', 'none');
  $('ol').on('click', 'li', function(){
    $(this).toggleClass('selected');
    $(this).find('.dropdown').fadeToggle(500);
  });

  //popover for icon selection in edit/add event
  $('.choose-icon-wrapper').css('display', 'none');
  $('.icon-wrapper').on('click', '.icon, ul li img', function(){
    $('.choose-icon-wrapper').fadeToggle(500);
  });

  //popover for link insertion in edit/add event
  $('.insert-link-wrapper').css('display', 'none');
  $('.link-wrapper').on('click', '.insert, a', function(){
    $('.insert-link-wrapper').fadeToggle(500);
  });

  //reveal slide for repeat events in edit/add event
  $('.frequency-wrapper').hide();
  $('#repeats').change(function(e){
    e.preventDefault();
    $('.frequency-wrapper').slideToggle(500);
  });
});