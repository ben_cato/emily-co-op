function initialize() {
  var myLatlng = new google.maps.LatLng(34.041056,-84.570794);
  var mapOptions = {
    zoom: 17,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [
      {
        "featureType": "landscape",
        "stylers": [
          { "color": "#ffffff" }
        ]
      },{
        "featureType": "road",
        "stylers": [
          { "color": "#808080" },
          { "saturation": -100 }
        ]
      },{
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
          { "weight": 0.1 },
          { "color": "#ffffff" }
        ]
      },{
        "featureType": "poi.school",
        "stylers": [
          { "color": "#dddddd" }
        ]
      },{
        "featureType": "poi.business",
        "elementType": "labels.text",
        "stylers": [
          { "color": "#808080" },
          { "weight": 0.1 }
        ]
      },{
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
          { "color": "#eaeaea" }
        ]
      },{
        "featureType": "water",
        "stylers": [
          { "color": "#ff0000" }
        ]
      },{
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
          { "color": "#dddddd" }
        ]
      }
    ],
    panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
    },
    zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.TOP_RIGHT
    },
    scaleControl: false,
    scrollwheel: false,
    mapTypeControl: false
  }
  var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'BrainJocks'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addListener(window, 'load', function() {
  var w = $('.' + map_wrapper_class + ' .bj-map-info').width();
  var h = $('.' + map_wrapper_class + ' .bj-map-info').height();
  $('.' + map_wrapper_class + ' .bj-map-info')
    .css("left",x_cord - w - 25)
    .css("top",y_cord - h / 2)
    .fadeIn();
});