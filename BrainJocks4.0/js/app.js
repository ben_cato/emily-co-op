//set body to 0 for fade in
TweenMax.set($('body'), {opacity: 0});

var tl = new TimelineMax();

//main navigation menu
var mainNavigation = function(){
  $('.menu').click(function(){
    $('.overlay').fadeToggle('fast', 'linear');
  });
}

//fades in body
var bodyFade = function(){
  var body = $('body');
  TweenMax.to(body, 1, {opacity: 1, ease:Power1.easeOut});
}

//fades in introduction text over hero image
var introduction = function(){
  var introText = $('.background h2');

  tl.staggerFrom(introText, 1, {opacity: 0, delay: 1.25, left: '-=60'}, 0.4);
}

//fades in/out header
var sweetHeader = function(){
  var header = $('header'),
      logo = $('header .logo'),
      nav = $('.overlay ul');

  TweenMax.set('header', {y: 0});
  TweenMax.set(logo, {scale: 1.3, transformOrigin: "0 0"});

  $('.page-wrapper').waypoint(function(){
    TweenMax.to(header, .5, {y: 0, force3D:true, ease: Power1.easeOut, backgroundColor: "rgba(34,34,34,0)"});
    TweenMax.to(logo, .5, {scale: 1.3});
    TweenMax.to(nav, .5, {top: 260, ease: Power1.easeOut});
  },
  {offset: 200});

  $('.page-wrapper').waypoint(function(up){
    TweenMax.to(header, .5, {y: -100, force3D:true, ease: Power1.easeOut, backgroundColor: "rgba(34,34,34,1)"});
    TweenMax.to(logo, .5, {scale: 1});
    TweenMax.to(nav, .5, {top: 115, ease: Power1.easeOut});
  },
  {offset: 200});
}

//fades in/out highlights
var highlightImageFade = function(){
  var image = $('.highlight img');
  TweenMax.set(image, {opacity: .1});

  $('.page-wrapper').waypoint(function(up){
    TweenMax.to(image, 1.5, {opacity: .1});
  },
  {offset: 400});

  $('.page-wrapper').waypoint(function(){
    TweenMax.to(image, 1.5, {opacity: 1});
  },
  {offset: 400});
}

//moves background image
var backgroundSlide = function(){
  var background = $('.animate');
  background.mouseenter(function() {
    TweenMax.to(background, 1, {backgroundPosition: '-300px 0', delay: 0.1, ease:Power1.easeOut});
  });
  background.mouseleave(function() {
    TweenMax.to(background, 1, {backgroundPosition: '-1px 0', delay: 0.1, ease:Power1.easeOut});
  });
}