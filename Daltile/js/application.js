$(window).load(function() {
	
    $('.dropdown-menu').on({
    "click":function(e){
        e.stopPropagation();
      }
    });

	$(function(){
		$('input[type="radio"]').on('click', function() {
			if($('#email-radio').is(':checked'))
			{
				$('#contact-info-label').text("Email:");
				$('#contact-info-input').attr("placeholder", "name@email.com");
			}
			else if($('#phone-radio').is(':checked'))
			{
				$('#contact-info-label').text("Phone:");
				$('#contact-info-input').attr("placeholder", "555-555-5555");
			}
		});
	});
});