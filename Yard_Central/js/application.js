﻿$(window).load(function(){

  //initialize flexslider
  $('.flexslider').flexslider({
    animation: "slide",
    animationSpeed: 1100,
    slideshow: false,
    controlNav: false,
    directionNav: false
  });  

  //connect slider buttons to calendar slides
  $('.previous').on('click', function(){
      $('.flexslider.calendar-picker').flexslider('prev')
      return false;
  })
  $('.next').on('click', function(){
      $('.flexslider.calendar-picker').flexslider('next')
      return false;
  })

  //connect slider buttons to month name slides
  $('.previous').on('click', function(){
      $('.flexslider.calendar-day').flexslider('prev')
      return false;
  })
  $('.next').on('click', function(){
      $('.flexslider.calendar-day').flexslider('next')
      return false;
  })

  //dropdown for multiple events
  $('.event-popover').css('display', 'none');
  $('.week').on('click', 'li', function(){
    $(this).toggleClass('selected');
    $(this).find('.event-popover').fadeToggle(500);
  });

  $('.close-popover').on('click', function(){
    $('.event-popover').css('display: none');
  });

  //gallery detail
  $('.gallery-detail').css('display', 'none');
  $('.gallery-cell').on('click', function(){
    $('.gallery-detail').hide();
    $(this).closest('.gallery-detail').fadeToggle(500);
  });

  //popover for link insertion in edit/add event
  $('.insert-link-wrapper').css('display', 'none');
  $('.link-wrapper').on('click', '.insert, a', function(){
    $('.insert-link-wrapper').fadeToggle(500);
  });

  //reveal slide for repeat events in edit/add event
  $('.frequency-wrapper').hide();
  $('#repeats').change(function(e){
    e.preventDefault();
    $('.frequency-wrapper').slideToggle(500);
  });
});